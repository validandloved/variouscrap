
-- make it so you can craft oil extract from sunflower seeds
minetest.register_craft({
	type = "shapeless",
	output = "basic_materials:oil_extract 2",
	recipe = {"group:food_sunflower_seeds", "group:food_sunflower_seeds", "group:food_sunflower_seeds", "group:food_sunflower_seeds", "group:food_sunflower_seeds", "group:food_sunflower_seeds"}
})

-- make it so you can craft oil extract from hemp seeds
minetest.register_craft({
	type = "shapeless",
	output = "basic_materials:oil_extract 2",
	recipe = {"farming:seed_hemp", "farming:seed_hemp", "farming:seed_hemp", "farming:seed_hemp", "farming:seed_hemp", "farming:seed_hemp"}
})
