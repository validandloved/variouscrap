-- various chat message commands and such

-- Join our Discord
minetest.register_chatcommand("discord", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "Visit our Discord at: https://discord.gg/JRptCjbkDZ !"
    end,
})

-- Check out our git
minetest.register_chatcommand("git", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        return true, "Check out our git at: https://gitlab.com/validandloved/ !"
    end,
})
