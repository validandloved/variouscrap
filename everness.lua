-- insert something to test if everness exists and if so do this stuff, if not then don't

-- everness chest to default chest
minetest.register_craft({
    output = 'default:chest',
    type = 'shapeless',
    recipe = {
        'everness:chest',
    }
})

-- everness dirt_1 to dirt
minetest.register_craft({
    output = 'default:dirt',
    type = 'shapeless',
    recipe = {
        'everness:dirt_1',
    }
})
