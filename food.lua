-- for food stuff
-- needs farming and x_farming mods

------------------------------------------------------------------------
-- baguette (farming)
minetest.register_tool("variouscrap:baguette", {
		description = ("A hard piece of bread"),
		wield_scale = {x=1.1,y=1.1,z=1.1},
		inventory_image = "variouscrap_baguette.png",
		wield_image = "variouscrap_baguette.png",
		on_secondary_use = minetest.item_eat(9),
		range = 3.75,
		groups = {food_bread = 1},
		tool_capabilities = {
			full_punch_interval = 0.85,
			max_drop_level=1,
			groupcaps={
			snappy={times={[1]=0.95, [2]=0.45, [3]=0.15}, uses=2, maxlevel=3},
			},
			damage_groups = {fleshy=1},
	},
})

minetest.register_craftitem("variouscrap:baguette_dough", {
	description = ("Baguette dough"),
	inventory_image = "variouscrap_baguette_dough.png",
	groups = {food_flour = 1}
})


minetest.register_craft({
	type = "shaped",
	output = "variouscrap:baguette_dough",
	recipe = {
        {"group:food_flour", "group:food_salt", "group:food_mixing_bowl"},
        {"group:water_bucket", "", ""}
	},
	replacements = {
		{"group:food_mixing_bowl", "farming:mixing_bowl"},
		{"group:water_bucket", "bucket:bucket_empty"}
	}
})


minetest.register_craft({
	type = "cooking",
	output = "variouscrap:baguette",
	recipe = "variouscrap:baguette_dough",
	cooktime = 40,
})

------------------------------------------------------------------------



------------------------------------------------------------------------
-- rice dishes (x_farming)

-- bowl plain rice
minetest.register_craftitem("variouscrap:plain_rice_bowl", {
	description = ("Plain bowl of rice"),
	drawtype = "plantlike",
	inventory_image = "variouscrap_plain_rice_bowl.png",
	wield_image = "variouscrap_plain_rice_bowl.png",
	on_use = minetest.item_eat(2, "farming:bowl"),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "variouscrap:plain_rice_bowl",
	recipe = {
		{"group:food_rice", "group:food_salt", "group:food_bowl"},
	}
})


-- bowl of buttered rice (x_farming and cheese or mobs_animal)
minetest.register_craftitem("variouscrap:buttered_rice_bowl", {
	description = ("Buttered bowl of rice"),
	drawtype = "plantlike",
	inventory_image = "variouscrap_buttered_rice_bowl.png",
	wield_image = "variouscrap_buttered_rice_bowl.png",
	on_use = minetest.item_eat(4, "farming:bowl"),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "variouscrap:buttered_rice_bowl",
	recipe = {
		{"group:food_butter", "", ""},
		{"group:food_rice", "group:food_salt", "group:food_bowl"},
	}
})


-- bowl of vegan buttered rice (x_farming and cheese)
minetest.register_craftitem("variouscrap:vegan_buttered_rice_bowl", {
	description = ("Vegan buttered bowl of rice"),
	drawtype = "plantlike",
	inventory_image = "variouscrap_buttered_rice_bowl.png",
	wield_image = "variouscrap_buttered_rice_bowl.png",
	on_use = minetest.item_eat(5, "farming:bowl"),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "variouscrap:vegan_buttered_rice_bowl",
	recipe = {
		{"cheese:vegetable_butter", "", ""},
		{"group:food_rice", "group:food_salt", "group:food_bowl"},
	}
})


-- bowl of sugared rice (x_farming and cheese or mobs_animal)
minetest.register_craftitem("variouscrap:sugared_rice_bowl", {
	description = ("Sugared bowl of rice"),
	drawtype = "plantlike",
	inventory_image = "variouscrap_sugared_rice_bowl.png",
	wield_image = "variouscrap_sugared_rice_bowl.png",
	on_use = minetest.item_eat(3, "farming:bowl"),
	groups = {flammable = 2, compostability = 65}
})

minetest.register_craft({
	output = "variouscrap:sugared_rice_bowl",
	recipe = {
		{"group:food_sugar", "", ""},
		{"group:food_rice", "group:food_salt", "group:food_bowl"},
	}
})


------------------------------------------------------------------------

------------------------------------------------------------------------

-- salt
minetest.register_craft({
	output = "farming:salt",
	recipe = {
		{"caverealms:salt_crystal"},
		{"farming:mortar_pestle"}
	},
	replacements = {{"group:food_mortar_pestle", "farming:mortar_pestle"}}
})

minetest.register_craft({
	output = "farming:salt",
	recipe = {
		{"caverealms:salt_gem"},
		{"farming:mortar_pestle"}
	},
	replacements = {{"group:food_mortar_pestle", "farming:mortar_pestle"}}
})

minetest.register_craft({
	output = "farming:salt",
	recipe = {
		{"caverealms:stone_with_salt"},
		{"farming:mortar_pestle"}
	},
	replacements = {{"group:food_mortar_pestle", "farming:mortar_pestle"}}
})

minetest.register_craft({
	output = "farming:salt",
	recipe = {
		{"everness:mineral_stone"},
		{"farming:mortar_pestle"}
	},
	replacements = {{"group:food_mortar_pestle", "farming:mortar_pestle"}}
})

------------------------------------------------------------------------

------------------------------------------------------------------------
