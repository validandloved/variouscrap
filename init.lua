local modpath = minetest.get_modpath(minetest.get_current_modname())

variouscrap = { }

dofile(modpath .. "/everness.lua")
dofile(modpath .. "/chatmessages.lua")
dofile(modpath .. "/food.lua")
dofile(modpath .. "/snowshovel.lua")
dofile(modpath .. "/basicmaterials.lua")
dofile(modpath .. "/sandwiches.lua")
