

-- make it so you can craft oil extract from peanuts
minetest.register_craft({
	type = "shapeless",
	output = "basic_materials:oil_extract 1",
	recipe = {"sandwiches:peanuts", "sandwiches:peanuts", "sandwiches:peanuts", "sandwiches:peanuts", "sandwiches:peanuts", "sandwiches:peanuts"}
})
