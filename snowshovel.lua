-- Snow shovel because getting rid of snow pileup sucks without it


minetest.register_tool("variouscrap:snowshovel", {
	description = ("Snow Removal Shovel"),
	inventory_image = "variouscrap_snowshovel.png",
	wield_image = "variouscrap_snowshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=3,
		groupcaps={
			snowy = {times={[1]=0.55, [2]=0.25, [3]=0.15}, uses=15, maxlevel=3},
		},
		damage_groups = {weather_snow_cover=1},
	},
	groups={snowshovel=1},
})

minetest.register_craft({
	output = "variouscrap:snowshovel",
	recipe = {
		{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
		{"default:steel_ingot", "default:stick", "default:steel_ingot"},
		{"", "default:stick", ""}
	}
})
